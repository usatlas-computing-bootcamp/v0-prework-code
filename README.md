# v0 PreWork Code

This package is intended as a starting point that includes a very basic event loop which 
inspects the jets in a given set of events.  Then input file path is hardcoded and should
be changed by the user for their specific setup.

# Compilation

In either case, the user should start by pulling the ATLAS `AnalysisBase,21.2.75` docker image
and entering into the docker image as below
```
docker run --rm -it -v $PWD:/home/atlas/Bootcamp atlas/analysisbase:21.2.75 bash
```
which will place the entire file structure within the `/home/atlas/Bootcamp` directory
of the image.  Next, choose to compile using either of the following two methods

## Standalone
It is possible to perform the compilation and linking in a standalone way, but it is a very
long command.  There are three pieces

Setup the g++ commands
```
source standalone_setup.sh
```
Perform the compilation to produce an object library
```
source standalone_compile.sh
```
Link this object library into a local executable
```
source standalone_link.sh
```
This will create `AnalysisPayload` which is executable.

## CMake
In this case, go one level above the directory of the repository and create a build directory.
In this directory, perform the CMake configuration and compilation
```
cd ..
mkdir build
cd build
cmake ../v0-prework-code
make
```
which will produce a locally executable `AnalysisPayload` again.

# Output

This code will simply print the kinematics of each jet in each event.